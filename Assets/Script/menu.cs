using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menu : MonoBehaviour
{
    // Start is called before the first frame update
    public Animator FadeAnimator;

    public void SceneLoadGame()
    {
        FadeAnimator.enabled = true;
    }

    public void FadeOutAnimatorEnded()
    {
        SceneManager.LoadScene("Cambio");
    }
}
