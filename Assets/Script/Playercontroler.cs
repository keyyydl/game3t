using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playercontroler : MonoBehaviour
{
    public Rigidbody2D PlayerRigibody;

    public Animator PlayerAnimator;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            PlayerRigibody.velocity = Vector2.left;
        }   
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            PlayerRigibody.velocity = Vector2.right;
        }
        else
        {
            PlayerRigibody.velocity = Vector2.zero;
        }

        PlayerAnimator.SetFloat("speed", PlayerRigibody.velocity.magnitude);
    }
}
